var TelegramBot = require('node-telegram-bot-api');
var DeepAffects = require('deep-affects');

//var token = '540996861:AAGxBqggqHyeFgwZTJjhbdB77a3oaEle8Yw';
var token = '719863537:AAE5jSnnMBhCXGyYR3Ba5Z10kWLPpeokJ2I';
var bot = new TelegramBot(token, { polling: true });

var defaultClient = DeepAffects.ApiClient.instance;
var UserSecurity = defaultClient.authentications['UserSecurity'];
UserSecurity.apiKey = 'wLKTX6bIvbIi1o7NyU2AyqMdut9YKGxJ';

var notes = [];

bot.onText(/\/start/, function(msg, match) {
    var userId = msg.from.id;

    console.log(msg);

    bot.sendMessage(userId, 'Speak!');
});

bot.onText(/\/help/, function(msg, match) {
    var userId = msg.from.id;

    console.log(msg);

    bot.sendMessage(userId, 'Send voice message here');
});

bot.on('message', msg => {
    const chatId = msg.chat.id;
    console.log(msg);

    if (msg.voice) {
        bot.sendMessage(chatId, 'Voice is analyzed...');
        console.log('deep affect works...');

        //bot.getFileLink(msg.voice.file_id)
        bot.downloadFile(msg.voice.file_id, './voices').then(link => {
            console.log(link);
            // bot.sendMessage(chatId, res.text())
            
            var apiInstance = new DeepAffects.EmotionApi();
            var body = DeepAffects.Audio.fromFile(link, msg.voice.mime_type);

            var callback = function(error, data, response) {
                if (error) {
                    console.error(error);
                } else {
                    console.log('from ' + msg.from.username);
                    console.log('API called successfully. Returned data: ');
                    console.log(data);
                    bot.sendMessage(chatId, charmAnswerOnEmoji(data));
                }
            };
            apiInstance.syncRecogniseEmotion(body, callback);
        });
    } else {
        bot.sendMessage(chatId, 'Voice not received');
    }

    // send a message to the chat acknowledging receipt of their message
    bot.sendMessage(chatId, 'aaaa');
});

function maxEmoji(arEmjs) {
    return arEmjs
        .sort((a, b) => {
            return a.score < b.score ? true : false;
        })
        .map(el => el.emotion + '  (' + el.score + ')')
        .slice(0, 3)
        .join(' > ');
}

function charmAnswerOnEmoji(arEmjs) {
    let emotion = arEmjs
        .sort((a, b) => {
            return a.score < b.score ? true : false;
        })
        .map(el => el.emotion)
        .slice(0, 1);

    if (emotion == 'angry') {
        return 'Осторожно! Собеседник зол и раздражителен!';
    } else if (emotion == 'disgust') {
        return 'Сорян, но ты кажется не в её/его вкусе...';
    } else if (emotion == 'fear') {
        return 'Спроси в чём дело, он/она напуган/а';
    } else if (emotion == 'happy') {
        return 'Светится от счастья, как голый зад при луне!';
    } else if (emotion == 'neutral') {
        return 'Кажись, ты её/его не возбуждаешь...';
    } else if (emotion == 'surprise') {
        return 'Ого!';
    } else if (emotion == 'sad') {
        return 'Поддержи морально собеседника или добей окончательно..';
    }
}
